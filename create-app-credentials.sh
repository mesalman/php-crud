#!/bin/bash
# This script reads simpleapp.env and creates equivalent kubernetes secrets.
# It needs to be capable for creating k8s secrets by reading ENV variables as well,
#   as, that is the case with CI systems.

CREDENTIALS_FILE=./php-crud-app.env
K8S_SECRET_NAME=php-crud-credentials

if [ ! -f ${CREDENTIALS_FILE} ]; then
  echo "Could not find ENV variables file. The file is missing: ${CREDENTIALS_FILE}"
  exit 1
fi

if [ ! -z "$1" ] ; then
  NAMESPACE=" --namespace=$1 "
else
  NAMESPACE=""
fi 

echo "First, deleting the old secret: ${K8S_SECRET_NAME}"
kubectl ${NAMESPACE} delete secret ${K8S_SECRET_NAME} || true

source ${CREDENTIALS_FILE}

echo "Found ${CREDENTIALS_FILE} file, creating kubernetes secret: ${K8S_SECRET_NAME}"

echo "MYSQL_HOST=${MYSQL_HOST}"
echo "MYSQL_DATABASE=${MYSQL_DATABASE}"
echo "MYSQL_USER=${MYSQL_USER}"
echo "MYSQL_PASSWORD=${MYSQL_PASSWORD}"

kubectl ${NAMESPACE} create secret generic ${K8S_SECRET_NAME} \
  --from-literal=MYSQL_HOST=${MYSQL_HOST} \
  --from-literal=MYSQL_DATABASE=${MYSQL_DATABASE} \
  --from-literal=MYSQL_USER=${MYSQL_USER} \
  --from-literal=MYSQL_PASSWORD=${MYSQL_PASSWORD}
